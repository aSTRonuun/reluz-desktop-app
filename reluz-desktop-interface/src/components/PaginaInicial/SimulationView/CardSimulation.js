import * as React from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { AddCircleOutline } from '@mui/icons-material';
import { CardActions } from '@mui/material';
import { Button } from '@mui/material';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import axios from 'axios';
import { Link } from 'react-router-dom';

export default function CardSimulation(props) {

    const handleDelete = async() => {
        
        try {

            const response = await axios.delete(`https://localhost:61319/api/Simulation/${props.id}`);

            if(response.status === 200){
                alert('Simulação deletada com sucesso!');

                
                props.getSimulations();
            }

        } catch (error) {
            
            console.log(error);
        }
    }

    return (
        <Card
        sx={{ minWidth: 275 }}
        >
            <CardContent>
                <Box sx={{ display: 'flex', flexDirection: 'row', justifyContent: 'center' }}>
                    <Typography variant="h6" >
                        {props.title}
                    </Typography>
                </Box>
                <Box sx={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Typography variant="body2">
                        Criado em: {props.createdAt}
                    </Typography>
                    <Typography variant="body2" >
                        Modificado em: {props.updatedAt}
                    </Typography>
                </Box>
            </CardContent>
            <CardActions 
                sx={{ 
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                }}
            >
                <Link 
                    to={`/simulacao/${props.id}`}
                    style={{ textDecoration: 'none' }}
                >
                    <Button
                        size="small"
                        variant="outlined"
                        color="primary"
                        startIcon={<EditIcon />}
                        onClick={() => {
                            window.location.href = `/simulacao/${props.id}`;
                        }}
                    >
                        Acessar
                    </Button>
                </Link>
                <Button 
                    size="small"
                    onClick={handleDelete}
                    variant="contained"
                    color="error"
                    startIcon={<DeleteIcon />}
                >
                    Excluir
                </Button>
            </CardActions>
        </Card>
  );
}