import { Card, Grid, Typography } from "@mui/material"
import { Box } from "@mui/system"
import React from 'react';
import css from "./index.css";
import BasicCard from "./CardAddSimulation";
import CardAddSimulation from "./CardAddSimulation";
import { Link } from "react-router-dom";
import BasicModal from "../modalFormSimulation/modalForm";
import axios from "axios";
import { useEffect } from "react";
import CardSimulation from "./CardSimulation";
import NavigationBread from "../Navigation/Navigation";



const SimulationView = () => {
    const [open, setOpen] = React.useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    const [inputSimulation, setInputSimulation] = React.useState({
        title: '',
    });
    const [simulations, setSimulations] = React.useState([]);
    const getSimulations = async() => {
        try {
            const user = JSON.parse(localStorage.getItem('user'));
            console.log(user);

            const response = await axios.get(`https://localhost:61319/api/Simulation/FindUserId/${user.id}`);

            setSimulations(response.data);
            localStorage.setItem('simulations', JSON.stringify(response.data));
        } catch (error) {
            console.log(error);
        }
    }

    useEffect(() => {
        getSimulations();
    }, []);


    const handleSubmit = (e) => {
        e.preventDefault();

        const user = JSON.parse(localStorage.getItem('user'));
        
        axios.post('https://localhost:61319/api/Simulation', {
            title: inputSimulation.title,
            userId: user.id,
        })
        .then((response) => {
            if (response.status === 200) {
                getSimulations();
                handleClose();
                handleClose();
            }
        }, (error) => {
            console.log(error);
        });
    }

    const handleChange = (e) => {
        console.log(e.target.value);
        console.log(e.target.name);
        setInputSimulation((prevState) => ({
          ...prevState,
          [e.target.name]: e.target.value,
        }));
      };

      return (
        <div className="containerSimulationView">
            <Box sx={{ flexGrow: 1 }}>
                <NavigationBread  cor1={true} />
            </Box>
            <Typography variant="h4" gutterBottom marginLeft={5}>
                Simulações
            </Typography>
            <BasicModal 
                open={open} 
                handleClose={handleClose} 
                handleSubmit={handleSubmit}
                handleChange={handleChange}
                value={inputSimulation.title}
                />
            <Grid container spacing={3} padding={5} columns={12}>
                <Grid item xs={8}>
                    <Link
                        onClick={handleOpen}
                        style={{ textDecoration: 'none' }}
                    >
                        <CardAddSimulation/>
                    </Link>
                </Grid>
                <Grid item xs={4}>
                    <CardAddSimulation/>
                </Grid>
            </Grid>

            <Typography variant="h5" gutterBottom marginLeft={5}>
                Suas simulações Simulações
            </Typography>

            {simulations.length === 0 ? 
            <Typography variant="h6" gutterBottom marginLeft={5}>
                Você não possui nenhuma simulação cadastrada
            </Typography> : 
            (
                <Grid container spacing={7} padding={4} columns={12}>
                    {simulations.map((simulation) => (
                        console.log(simulation),
                        <Grid item xs={4}>
                            <CardSimulation
                                title={simulation.title}
                                createdAt={simulation.createdAt}
                                updatedAt={simulation.updatedAt}
                                id={simulation.simulationId}
                                getSimulations={getSimulations}
                            />
                        </Grid>
                    ))}
                </Grid>
            )
            }
        </div>
    );
}

export default SimulationView