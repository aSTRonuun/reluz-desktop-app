import * as React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import { TextField } from '@mui/material';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

export default function BasicModal( {open, handleClose, handleSubmit, handleChange, title} ) {
  return (
    <div>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <form onSubmit={handleSubmit}>
            <Typography id="modal-modal-title" variant="h6" component="h2">
              Adicionar nova Simulação
            </Typography>
            <TextField
              required
              name='title'
              id="outlined-required"
              label="Nome da Simulação"
              onChange={handleChange}
              value={title}
            />
            <Button type="submit" position="center">Adicionar</Button>
          </form>
        </Box>
      </Modal>
    </div>
  );
}