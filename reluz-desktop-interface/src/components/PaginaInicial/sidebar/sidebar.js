import './sidebar.css';
import logo from '../../../foto/logo.png'

import HomeIcon from '@mui/icons-material/Home';
import PersonIcon from '@mui/icons-material/Person';
import PaidIcon from '@mui/icons-material/Paid';
import PowerSettingsNewIcon from '@mui/icons-material/PowerSettingsNew';
import { Link } from 'react-router-dom';

const Sidebar = ( {sidebarOpen,closeSidebar} ) =>{

    return(
      <div className={sidebarOpen ? "sidebar-responsive" : ""} id="sidebar">
        <div className="sidebar__title">
          <div className="sidebar__img">
            <img src={logo} alt="logo" />
            <h1>Usuario</h1>
      </div>
            {/* mudar o className ae pra algo do UI ITENS que é similar */}
            {/* esse className fa fa-times eu n consegui modificar pra tipo <CloseIcon/> */}
            <i onClick={() => closeSidebar()}
            className="fa fa-times" 
            id="sidebarIcon"
            aria-hidden="true"
            ></i>
           </div> 
          <div className="sidebar__menu">
            <div className="sidebar__link active_menu_link">
                <i><HomeIcon/></i>
                <Link to="/dashboard">Dashboard</Link>
              </div>
           
                
               <div className="sidebar__link">
                <i><PersonIcon/></i>
                <Link to="/perfil">Perfil</Link>
              </div>
              
              <div className="sidebar__link">
                <i><PaidIcon/></i>
                <Link to="/simulacao">Simulaçôes</Link>
              </div>
               
               
              <div className="sidebar__logout">
                <i><PowerSettingsNewIcon/></i>
                <Link to="/">
                  Log Out 
                </Link>
              </div>
        </div>

      </div>
    ) 
}

export default Sidebar;