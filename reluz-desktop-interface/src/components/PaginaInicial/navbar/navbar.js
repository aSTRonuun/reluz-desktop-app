import './navbar.css';
import avatar from '../../../foto/avatar.png'
import SearchIcon from '@mui/icons-material/Search';
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import ReorderIcon from '@mui/icons-material/Reorder';

const Navbar = ({sidebarOpen, openSidebar}) =>{

  return(

    //esse className fa fa-times eu n consegui modificar pra tipo <CloseIcon/> o fa fa-bars
    //tabem escolher icones que sejam as barrinhas mas no UI MATERIAL
    <nav className="navbar">
      <div className="nav_icon" onClick={() => openSidebar()}>
        <i> <ReorderIcon aria-hidden="true"/></i> 
        
      </div>

      <div className="navbar__left">
        
      </div>

      <div className="navbar__right">
        <a href="#">
          <i><SearchIcon/></i>
        </a>          
        <a href="#">
          <i><AccessTimeIcon/></i>
        </a>          
        <a href="#">
          <img width="30" src={avatar} alt="avatar" />
        </a>
      </div>
    </nav>    
  )  
}
export default Navbar;