import hello from '../../../foto/hello.png'
import './main.css';
import Chart from '../charts/Chart'

import ArticleIcon from '@mui/icons-material/Article';
import PaidIcon from '@mui/icons-material/Paid';
import InventoryIcon from '@mui/icons-material/Inventory';
import ListIcon from '@mui/icons-material/List';
import AttachMoneyIcon from '@mui/icons-material/AttachMoney';
import ShowChartIcon from '@mui/icons-material/ShowChart';
import { red } from '@mui/material/colors';
import { yellow } from '@mui/material/colors';


const main = () =>{
  return(
    <main>
    <div className='main__container'>
      <div className='main__title'>
        <img src={hello} alt="hello" />
        <div className='main__greeting'>
          <h1>Olá Rodrigo</h1>
          <p>Bem vindo ao seu painel</p>
        </div>
      </div>

    <div className='main__cards'>
      <div className='card'>
        <i><ArticleIcon 
          color="primary"
          fontSize="large" 
        /></i>
        <div className='card_inner'>
          <p className="text-primary-p">Número de pedidos</p>
          <span className="font-bold text-title">578</span>
        </div>
      </div>


      <div className='card'>
      <i><PaidIcon 
          sx={{ color: red[500] }} 
          fontSize="large" 
        /></i>
        <div className='card_inner'>
          <p className="text-primary-p">Pagamentos</p>
          <span className="font-bold text-title">R$ 2.467</span>
        </div> 
      </div>

      <div className='card'>
      <i><InventoryIcon 
          sx={{ color: yellow[500] }} 
          fontSize="large" 
        /></i>
        <div className='card_inner'>
          <p className="text-primary-p">Número de produtos</p>
          <span className="font-bold text-title">670</span>
        </div> 
      </div>

      <div className='card'>
      <i><ListIcon 
          color="success"
          fontSize="large" 
        /></i>
        <div className='card_inner'>
          <p className="text-primary-p">Categorias</p>
          <span className="font-bold text-title">40</span>
        </div> 
      </div>
   </div>


   <div className='charts'>
    <div className='charts__left'>
      <div className='charts__left__title'>
        <div>
          <h1>Daily Reports</h1>
          <p>Quixada, Ceará, BR</p>
        </div>
        <i><AttachMoneyIcon/></i>
      </div>
      <Chart />
    </div>

    <div className='charts__right'>
      <div className='charts__right__title'>
        <div>
          <h1>Daily Reports</h1>
          <p>Quixada, Ceará, BR</p>
        </div>
        <i><ShowChartIcon/></i>
      </div>


      <div className='charts__right__cards'>
        <div className='card1'>
          <h3>Lucro</h3>
          <h3>R$2500</h3>
        </div>

        <div className='card2'>
          <h3>Pagamentos</h3>
          <h3>R$250,00</h3>
        </div>

        <div className='card3'>
          <h3>Custos de Hospegadem</h3>
          <h3>R$150,00</h3>
        </div>

        <div className='card4'>
          <h3>Banco de dados</h3>
          <h3>R$180,00</h3>
        </div>
      </div>
       
     </div>
    

   </div>
 
    </div>
    </main>
  )
}

export default main;