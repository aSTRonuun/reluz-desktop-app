import { Box, Grid, Typography } from "@mui/material";
import { Container } from "@mui/system";
import { useEffect, useState } from "react";
import ModalCalculationView from "./ModalCalculation1View";
import ModalCalculation1View from "./ModalCalculation1View";
import css from "./index.css";
import CardSimulation from "../SimulationView/CardSimulation";
import CardCalculation from "./CardCalculation";
import axios from "axios";
import NavigationBread from "../Navigation/Navigation";

const SimulationCalculationsView = () => {

    const [simulation, setSimulation] = useState({});
    const [calculation1, setCalculation1] = useState({});
    const [openC1, setOpenC1] = useState(false);
    const handleOpenC1 = () => {
        console.log("handleOpenC1");
        setOpenC1(true);
    };
    const handleCloseC1 = () => {
        console.log("handleCloseC1");
        setOpenC1(false);
    };

    const handleChangeC1 = (e) => {
        console.log(e.target.name);
        console.log(e.target.value);
        setCalculation1((prevState) => ({
            ...prevState,
            [e.target.name]: e.target.value,
        }));
    }

    useEffect(() => {
        getSimulation();
    }, []);

    const getSimulation = async() => {
        const simulations = JSON.parse(localStorage.getItem('simulations'));

        const simulation = simulations.find((simulation) => simulation.simulationId === parseInt(window.location.pathname.split('/')[2]));
        const calculation1 = simulation.calculation1;
        setSimulation(simulation);
        setCalculation1(calculation1);
        console.log(calculation1);
    }

    const handleSubmitC1 = async(e) => {
        e.preventDefault();

        simulation.calculation1 = calculation1;
        console.log(simulation);

        simulation.calculation1.averageConsumptionInReais = parseFloat(simulation.calculation1.averageConsumptionInReais);
        simulation.calculation1.sumConsumptionInKwPerMonth = parseFloat(simulation.calculation1.sumConsumptionInKwPerMonth);
        simulation.calculation1.solarRadiationIndex = parseFloat(simulation.calculation1.solarRadiationIndex);

        const response = await axios.put("https://localhost:61319/api/Simulation/UpdateCalculation1", simulation);

        const data = response.data;
        setSimulation(data);
        setCalculation1(data.calculation1);
    }
    

    return (
        <div className="containerSimulationCalculationView">
            <Box sx={{ flexGrow: 1 }}>
                <NavigationBread   cor2={true}/>
            </Box>
            <Typography variant="h4" gutterBottom marginLeft={5}>
                Simulação: {simulation.title}
            </Typography>
            <ModalCalculation1View
                openC1={openC1}
                handleCloseC1={handleCloseC1}
                handleChangeC1={handleChangeC1}
                handleSubmitC1={handleSubmitC1}
                calculation1={calculation1}
            />
            <Box sx={{ flexGrow: 1 }} marginLeft={5} marginTop={5}>
                <Grid container spacing={3} columns={12}>
                    <Grid item xs={4}>
                        <CardCalculation
                            title={calculation1.title}
                            descriptionn={calculation1.descriptionn}
                            handleOpenC1={handleOpenC1}
                            handleSubmitC1={handleSubmitC1}
                        />
                    </Grid>
                    <Grid item xs={4}>
                        <CardCalculation
                            title={"Cálculo 2"}
                            descriptionn={"Lorem ipsum, dolor sit amet consectetur adipisicing elit. Delectus iusto est architecto veritatis accusantium quam doloribus quaerat facilis! Nobis at dolorem officiis, totam eveniet dolore explicabo perferendis similique numquam earum."}
                        />
                        
                    </Grid>
                    <Grid item xs={4}>
                        <CardCalculation
                            title={"Cálculo 3"}
                            descriptionn={"Lorem ipsum, dolor sit amet consectetur adipisicing elit. Delectus iusto est architecto veritatis accusantium quam doloribus quaerat facilis! Nobis at dolorem officiis, totam eveniet dolore explicabo perferendis similique numquam earum."}
                        />
                    </Grid>
                    <Grid container spacing={7} padding={4} columns={12}>
                        <Grid item xs={6}>
                            <CardCalculation
                                title={"Cálculo 4"}
                                descriptionn={"Lorem ipsum, dolor sit amet consectetur adipisicing elit. Delectus iusto est architecto veritatis accusantium quam doloribus quaerat facilis! Nobis at dolorem officiis, totam eveniet dolore explicabo perferendis similique numquam earum."}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <CardCalculation
                                title={"Cálculo 5"}
                                descriptionn={"Lorem ipsum, dolor sit amet consectetur adipisicing elit. Delectus iusto est architecto veritatis accusantium quam doloribus quaerat facilis! Nobis at dolorem officiis, totam eveniet dolore explicabo perferendis similique numquam earum."}
                            />
                        </Grid>
                    </Grid>
                </Grid>
            </Box>
        </div>
    );
}

export default SimulationCalculationsView