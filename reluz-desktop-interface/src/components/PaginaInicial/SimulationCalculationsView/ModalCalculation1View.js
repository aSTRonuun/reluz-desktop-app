import * as React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import { Grid, TextField } from '@mui/material';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 600,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

export default function ModalCalculation1View( {openC1, handleCloseC1, handleSubmitC1, handleChangeC1, calculation1} ) {
  return (
    <div>
      <Modal
        open={openC1}
        onClose={handleCloseC1}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <form onSubmit={handleSubmitC1}>
            <Typography id="modal-modal-title" variant="h6" component="h2">
              {calculation1.title}
            </Typography>
            <Grid 
              paddingTop={2}
              container
              justifyContent="center"
              alignItems="center"
              spacing={2}
              columns={12}
            >
              <Grid item xs={6}>
                <TextField
                    required
                    type="number"
                    name='averageConsumptionInReais'
                    id="outlined-required"
                    label="Média do consumo em reais"
                    onChange={handleChangeC1}
                    value={calculation1.averageConsumptionInReais}
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  required
                  type="number"
                  name='sumConsumptionInKwPerMonth'
                  id="outlined-required"
                  label="Soma do consumo em kw por mês"
                  onChange={handleChangeC1}
                  value={parseFloat(calculation1.sumConsumptionInKwPerMonth)}
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  required
                  type="number"
                  name='solarRadiationIndex'
                  id="outlined-required"
                  label="Indice de Radiação Solar em KwH/m²"
                  onChange={handleChangeC1}
                  value={parseFloat(calculation1.solarRadiationIndex)}
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  disabled
                  type="number"
                  name='overallIncome'
                  id="outlined-required"
                  label="Rendimento global do sistema"
                  onChange={handleChangeC1}
                  value={calculation1.overallIncome}
                />
              </Grid>
              <Grid item xs={6}>
                <Typography variant='subtitle2'>
                  Geração do Sistema antigo em kWh/dia
                </Typography>
                <TextField
                  disabled
                  type="number"
                  name='systemGenerationInKwPerDay'
                  id="outlined-required"
                  onChange={handleChangeC1}
                  value={calculation1.systemGenerationInKwPerDay}
                />
              </Grid>
              <Grid item xs={6}>
                <Typography variant='subtitle2'>
                  Geração do Sistema novo em kWh/dia
                </Typography>
                <TextField
                  disabled
                  type="number"
                  name='systemGenerationWithBoardPerDay'
                  id="outlined-required"
                  onChange={handleChangeC1}
                  value={calculation1.systemGenerationWithBoardPerDay}
                />
              </Grid>
              <Grid item xs={12} display="flex" justifyContent="center">
                <Grid item xs={6}>
                  <Typography variant='subtitle2'>
                    Geração do Sistema novo em kWh/mês
                  </Typography>
                  <TextField
                    disabled
                    type="number"
                    name='systemGenerationWithBoardPerMonth'
                    id="outlined-required"
                    onChange={handleChangeC1}
                    value={calculation1.systemGenerationWithBoardPerMonth}
                  />
                </Grid>
              </Grid>
              <Grid
                  item
                  columns={12}
                >
                <Button type="submit" variant='outlined' position="center">Calcular</Button>
              </Grid>
            </Grid>
          </form>
        </Box>
      </Modal>
    </div>
  );
}