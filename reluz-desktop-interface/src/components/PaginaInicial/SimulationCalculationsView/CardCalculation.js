import * as React from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { AddCircleOutline } from '@mui/icons-material';
import { CardActions } from '@mui/material';
import { Button } from '@mui/material';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import axios from 'axios';
import { Link } from 'react-router-dom';

export default function CardCalculation({ title, descriptionn, handleOpenC1}) {
    return (
        <Card
        sx={{ minWidth: 275, height: 275 }}
        >
            <CardContent>
                <Box sx={{ display: 'flex', flexDirection: 'row', justifyContent: 'center' }}>
                    <Typography variant="h6" >
                        {title}
                    </Typography>
                </Box>
                <Box sx={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Typography variant="body2">
                        {descriptionn}
                    </Typography>
                </Box>
            </CardContent>
            <CardActions 
                sx={{ 
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                }}
            >
                <Button
                    size="small"
                    variant="outlined"
                    color="primary"
                    startIcon={<EditIcon />}
                    onClick={handleOpenC1}
                >
                    Acessar
                </Button>
            </CardActions>
        </Card>
  );
}