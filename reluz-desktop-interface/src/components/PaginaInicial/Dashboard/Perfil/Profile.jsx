import logo from "../../../../foto/logo.png";
import axios from "axios";

import TextField from "@mui/material/TextField";
import Stack from "@mui/material/Stack";
import Button from "@mui/material/Button";
import "./perfil.css";
import { useState } from "react";

const Profile = () => {
  const [isUpdate, setIsUpdate] = useState(true);

  const user = JSON.parse(localStorage.getItem("user"));

  const updatedSetIsUpdate = () => {
    setIsUpdate(!isUpdate);
    if (isUpdate) {
      inpust.password1 = "";
    } else {
      inpust.password1 = user.password;
    }
  };

  // const [isSignup, setIsSignup] = useState(false);
  const [inpust, setInputs] = useState({
    nameUser: user.userName,
    password1: "",
    password2: "",
    password3: "",
  });

  const handleChange = (e) => {
    console.log(e.target.value);
    console.log(e.target.name);

    setInputs((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }));
  };

  const userUpdate = async() => {
    
    const response = await axios.put(`https://localhost:61319/api/User`, {
      id: user.id,
      userName: inpust.nameUser,
      password: inpust.password1,
      newPassword: inpust.password2,
    });

    if (response.status === 200) {
      alert("Usuário atualizado com sucesso!");
      localStorage.setItem("user", JSON.stringify(response.data));
      updatedSetIsUpdate();
      inpust.password2 = "";
      inpust.password3 = "";
    } else {
      alert("Erro ao atualizar usuário!");
    }
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    
    console.log(inpust.password2);
    console.log(inpust.password3);
    // Se a senha atual eh igual a senha que o usuario digitou
    if (inpust.password2 === inpust.password3) {
      userUpdate();
      return
    }
    alert("As senhas novas não conferem, tente novamente!");
    return
  };

  return (
    <main>
      <h2 class="titulo">Perfil</h2>
      <div class="quadro-pefil">
        <form onSubmit={handleSubmit}>
          <Stack
            direction="column"
            justifyContent="center"
            alignItems="center"
            spacing={2}
          >
            <img src={logo} alt="logo" class="imagem-do-perfil" />

            <TextField //essa eh pra ser aquela caixa de texto la so que desabilitada
              {...(isUpdate && { disabled: true })}
              onChange={handleChange}
              name="nameUser"
              value={inpust.nameUser}
              type={"text"}
              id="filled-disabled"
              label="Nome de usário"
              variant="filled"
            />

            {!isUpdate && (
              <TextField //essa eh pra ser aquela caixa de texto la so que desabilitada
                {...(isUpdate && { disabled: true })}
                onChange={handleChange}
                name="password1"
                value={inpust.password1}
                type={"password"}
                id="filled-disabled"
                label="Senha Atual"
                required
                // defaultValue="Insira sua senha"
                variant="filled"
                //nao sei se seria bem essa ou number
              />
            )}

            {!isUpdate && (
              <TextField
                onChange={handleChange}
                name="password2"
                required
                value={inpust.password2}
                type={"password"}
                id="filled-disabled"
                label="Nova Senha"
                defaultValue="Insira sua senha"
                variant="filled"
              />
            )}

            {!isUpdate && (
              <TextField
                onChange={handleChange}
                name="password3"
                value={inpust.password3}
                required
                type={"password"}
                id="filled-disabled"
                label="Confirmar Nova Senha"
                defaultValue="Insira sua senha"
                variant="filled"
              />
            )}
            </Stack>

            <div class="espaco"></div>
            <Stack
            direction="row"
            justifyContent="space-evenly"
            alignItems="baseline"
            spacing={1}
            >
            {isUpdate && (
              <Button
              onClick={updatedSetIsUpdate} 
              // onClick={changeBotao,changeCor}
              variant="outlined"
              {...(isUpdate && { color: "warning" })}
              // {isUpdate ? "Atualizar" : "Confirmar"}
              >
              Atualizar
            </Button>
            )}

            {!isUpdate && (
              <Button
                type="submit"
                // onClick={changeBotao,changeCor}
                variant="outlined"
                {...(isUpdate && { color: "warning" })}
                // {isUpdate ? "Atualizar" : "Confirmar"}
                >
                Confirmar
              </Button>
            )}

            {isUpdate && (
                <Button
                {...(!isUpdate && { disabled: true })}
                variant="contained"
                color="error"
              >
                Deletar
              </Button>
            )}
            {!isUpdate && (
                <Button
                disabled={isUpdate}
                onClick={updatedSetIsUpdate}
                variant="contained"
              >
                Cancelar
              </Button>
            )}
          </Stack>
        </form>
      </div>
    </main>
  );
};

export default Profile;
