import * as React from 'react';
import Breadcrumbs from '@mui/material/Breadcrumbs';
import Link from '@mui/material/Link';
import { hover } from '@testing-library/user-event/dist/hover';

function handleClick(event) {
  console.info('You clicked a breadcrumb.');
}

export default function NavigationBread({cor1, cor2, }) {
  return (
    <div role="presentation" onClick={handleClick}>
      <Breadcrumbs aria-label="breadcrumb">
        <Link 
          underline="hover" 
          color={cor1 ? "blue" : "inherit"} 
          href="/simulacao">
          Simulação
        </Link>
        <Link
          underline="hover"
          color={cor2 ? "blue" : "inherit"}
          underline="none"
        >
          Calculos
        </Link>
      </Breadcrumbs>
    </div>
  );
}