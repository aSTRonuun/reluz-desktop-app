import React, { useState } from "react";
import { Box, TextField, Typography, Button }  from '@mui/material';
import LoginOutlinedIcon from '@mui/icons-material/LoginOutlined';
import HowToRegOutlinedIcon from '@mui/icons-material/HowToRegOutlined';
import axios from "axios";
import css from "./index.css";
import { Link } from "react-router-dom";

const Login = () => {

  const [isSignup, setIsSignup] = useState(false);
  const [inpust, setInputs] = useState({
    nameUser: '',
    password1: '',
    password2: '',
  })

  const handleChange = (e) => {
    console.log(e.target.value);
    console.log(e.target.name);

    setInputs((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }));
  };

  const resetState = () => {
    setIsSignup(!isSignup);
    setInputs({
      nameUser: '',
      password1: '',
      password2: '',
    });
  }

 
  const handleSubmit = (e) => {
    e.preventDefault();
    if (isSignup) {

      if (inpust.password1 !== inpust.password2) {
        alert('As senhas não conferem, tente novamente!');
        return;
      }

      axios.post('https://localhost:61319/api/User', {
        userName: inpust.nameUser,
        password: inpust.password1,
      })
        .then((response) => {
          if (response.status === 200) {
            alert('Usuário cadastrado com sucesso!');
            localStorage.setItem('user', JSON.stringify(response.data));
            resetState();
          }
        }, (error) => {
          console.log(error);
        });
    }
    else {
      console.log(inpust);
      axios.post('https://localhost:61319/api/User/Login', {
        userName: inpust.nameUser,
        password: inpust.password1,
      })
        .then((response) => {
          if (response.status === 200) {
            alert('Login realizado com sucesso!');
            // Aguardar dados do usuário
            localStorage.setItem('user', JSON.stringify(response.data));
            resetState();
            window.location.href = '/dashboard';
          } else if (response.status === 404) {
            alert('Usuário não encontrado!');
          }
        }, (error) => {
          console.log(error);
        });
    }
  };


  return (
    <div className="formContainerLogin">
      <form onSubmit={handleSubmit}>
        <Box 
          display="flex" 
          flexDirection={"column"} 
          minWidth={400} 
          alignItems="center"
          justifyContent="center"
          margin="auto"
          marginTop={5}
          padding={3}
          borderRadius={5}
          boxShadow={"5px 5px 10px #ccc"}
          bgcolor="#fff"
          sx={{
            ":hover" : {
              boxShadow: "10px 10px 20px #ccc"
            },
          }}
        >
          <Typography variant="h2" padding={3} textAlign="center">
            {isSignup ? "Registrar" : "Entrar"}
          </Typography>
          <TextField
            onChange={handleChange}
            name="nameUser"
            value={inpust.nameUser}
            margin="normal"
            required
            type={"text"} 
            variant="outlined" 
            placeholder="Nome de usário"/>
          <TextField
            onChange={handleChange}
            name="password1"
            value={inpust.password1}
            margin="normal"
            required
            type={"password"} 
            variant="outlined" 
            placeholder="Senha"/>
          {isSignup && (
            <TextField
              onChange={handleChange}
              name="password2"
              value={inpust.password2}
              required
              margin="normal" 
              type={"password"} 
              variant="outlined" 
              placeholder="Senha"
            />
          )}
          <Button
              endIcon={isSignup ? <HowToRegOutlinedIcon /> : <LoginOutlinedIcon />}

              type="submit"
              sx={{ marginTop: 3, borderRadius: 3}} 
              variant="contained" 
              color="warning"
            >
              {isSignup ? "Registrar" : "Entrar"}
          </Button>
          <Button
            endIcon={isSignup ? <LoginOutlinedIcon /> : <HowToRegOutlinedIcon />}
            onClick={resetState} 
            sx={{ marginTop: 3, borderRadius: 3}}>
              {isSignup ? "JÁ TEM CONTA? ENTRE" : "NÃO TEM CONTA? REGISTRE-SE"}
          </Button>
        </Box>
      </form>
    </div>
  )
}

export default Login;