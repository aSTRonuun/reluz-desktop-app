import "bootstrap/dist/css/bootstrap.min.css";
import Login from "./components/PaginaLogin/Login"
import { useState } from 'react';
import { Routes, Route, Link} from "react-router-dom";
import Navbar from './components/PaginaInicial/navbar/navbar'
import Sidebar from "./components/PaginaInicial/sidebar/sidebar"
import Main from "./components/PaginaInicial/main/main"
import './components/PaginaInicial/indexgeral.css'
import SimulationView from "./components/PaginaInicial/SimulationView/simulationView";
import SimulationCalculationsView from "./components/PaginaInicial/SimulationCalculationsView/simulationCalulationView";
import Profile from "./components/PaginaInicial/Dashboard/Perfil/Profile";



const App = () => {

  const [sidebarOpen, setSidebarOpen] = useState(false);
  const [simulationId, setSimulationId] = useState(0);
  const openSidebar = () =>{
    setSidebarOpen(true);
  };

  const closeSidebar = () =>{
    setSidebarOpen(false);
  };



  return (
    <div> 
  
    {/* <Link to="/createProfessor" className="nav-link">Criar Professor</Link> */}
    
  <Routes>

    <Route path ='/' element={ <Login/> }/>
 
    <Route path="/dashboard" element={
      <div className="containner">
      <Navbar sidebarOpen={sidebarOpen} openSidebar={openSidebar} />
      <Main/>
      <Sidebar sidebarOpen={sidebarOpen} closeSidebar={closeSidebar}/>
      </div>
    } />

    <Route path="/perfil" element={
      <div className="containner">
      <Navbar sidebarOpen={sidebarOpen} openSidebar={openSidebar} />
      <Profile/>         
      <Sidebar sidebarOpen={sidebarOpen} closeSidebar={closeSidebar}/>
      </div>
    } />

    <Route path="/simulacao" element={
      <div className="containner">
        <Navbar sidebarOpen={sidebarOpen} openSidebar={openSidebar} />
        <SimulationView />
        <Sidebar sidebarOpen={sidebarOpen} closeSidebar={closeSidebar}/>
      </div>
    } />

    <Route path="/simulacao/:id" element={
      <div className="containner">
        <Navbar sidebarOpen={sidebarOpen} openSidebar={openSidebar} />
        <SimulationCalculationsView/>
        <Sidebar sidebarOpen={sidebarOpen} closeSidebar={closeSidebar}/>
      </div>
    } />
  </Routes>
  </div>
  );
}

export default App;
