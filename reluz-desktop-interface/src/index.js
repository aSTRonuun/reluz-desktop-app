import React from 'react';
import './index.css';
import './components/PaginaInicial/indexgeral.css'

import App from './App';
import { BrowserRouter } from 'react-router-dom';
import * as ReactDOM from 'react-dom/client';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <BrowserRouter>
    <App />
  </BrowserRouter>
);