﻿using System.Security.Cryptography;
using System.Text;

namespace reluz_desktop_server.Service.Cryptography
{
    public static class Cryptography
    {
        public static string GenerateHash(this string value)
        {
            if (string.IsNullOrEmpty(value)) { return new ArgumentException(nameof(value)).Message; }

            var hash = SHA1.Create();
            var encoding = new ASCIIEncoding();
            var array = encoding.GetBytes(value);

            array = hash.ComputeHash(array);

            var strHexa = new StringBuilder();

            foreach (var item in array)
            {
                strHexa.Append(item.ToString("x2"));
            }

            return strHexa.ToString();
        }
    }
}
