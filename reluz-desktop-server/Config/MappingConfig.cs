﻿using AutoMapper;
using reluz_desktop_server.Data.Dtos;
using reluz_desktop_server.Model;
using reluz_desktop_server.Model.SimulationClasses;
using reluz_desktop_server.Model.SimulationClasses.Calculations;

namespace reluz_desktop_server.Config;

public class MappingConfig
{
    public static MapperConfiguration RegisterMaps()
    {
        var mappingConfig = new MapperConfiguration(config =>
        {
            config.CreateMap<User, UserDto>().ReverseMap();
            config.CreateMap<Simulation, SimulationDto>()
                .ForMember(dest => dest.calculation1, opt => opt.MapFrom(src => src.Calculation1));
            config.CreateMap<SimulationDto, Simulation>()
                .ForMember(dest => dest.Calculation1, opt => opt.MapFrom(src => src.calculation1));
            config.CreateMap<Calculation1, Calculation1Dto>().ReverseMap();
        });

        return mappingConfig;
    }
}
