﻿namespace reluz_desktop_server.Data.Dtos
{
    public class Calculation1Dto
    {
        public int? Id { get; set; }
        public string Title { get; set; }
        public string Descriptionn { get; set; }
        public int SimulationId { get; set; }
        public double? AverageConsumptionInReais { get; set; }
        public double? SumConsumptionInKwPerMonth { get; set; }
        public double? SystemGenerationInKwPerDay { get; set; }
        public double? OverallIncome { get; set; }
        public double? SolarRadiationIndex { get; set; }
        public double? SystemGenerationWithBoardPerDay { get; set; }
        public double? SystemGenerationWithBoardPerMonth { get; set; }
    }
}
