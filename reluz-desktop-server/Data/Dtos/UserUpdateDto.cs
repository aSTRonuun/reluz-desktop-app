﻿namespace reluz_desktop_server.Data.Dtos
{
    public class UserUpdateDto
    {
        public int Id { get; set; }
        public string? UserName { get; set; }
        public string? Password { get; set; }
        public string? NewPassword { get; set; }
    }
}
