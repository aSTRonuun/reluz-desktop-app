﻿namespace reluz_desktop_server.Data.Dtos;

public class UserDto
{
    public long Id { get; set; }
    public string UserName { get; set; }
    public string Password { get; set; }
}
