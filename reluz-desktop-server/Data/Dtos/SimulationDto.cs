﻿using reluz_desktop_server.Model.SimulationClasses.Calculations;

namespace reluz_desktop_server.Data.Dtos
{
    public class SimulationDto
    {
        public int SimulationId { get; set; }
        public string Title { get; set; }
        public string? CreatedAt { get; set; }
        public string? UpdatedAt { get; set; }
        public int UserId { get; set; }
        public Calculation1Dto? calculation1 { get; set; }
    }
}
