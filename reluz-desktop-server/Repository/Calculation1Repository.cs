﻿using Microsoft.EntityFrameworkCore;
using reluz_desktop_server.Model.Context;
using reluz_desktop_server.Model.SimulationClasses.Calculations;
using reluz_desktop_server.Repository.IRepository;

namespace reluz_desktop_server.Repository
{
    public class Calculation1Repository : ICalculation1Repository
    {
        private readonly MySQLContext _context;

        public Calculation1Repository(MySQLContext context)
        {
            _context = context;
        }

        public async Task<Calculation1> Create(int simulationId)
        {
            var calculation1 = new Calculation1
            {
                Title = "Geração do Sistema de Energia Solar em Kw",
                Descriptionn = "Calculo utilizado para saber quanto de energia será necessária gerar po dia e mês com placas solares para suprir a necessidade do usuário.",
                SimulationId = simulationId,
                AverageConsumptionInReais = 0,
                SumConsumptionInKwPerMonth = 0,
                SystemGenerationInKwPerDay = 0,
                SolarRadiationIndex = 0,
                OverallIncome = 0.7,
                SystemGenerationWithBoardPerDay = 0,
                SystemGenerationWithBoardPerMonth = 0
            };

            _context.Calculations1.Add(calculation1);
            await _context.SaveChangesAsync();
            return calculation1;
        }

        public async Task<Calculation1> FindBySimulationId(int simulationId)
        {
            var calculation = await _context
                .Calculations1
                .Where(c => c.SimulationId == simulationId)
                .FirstOrDefaultAsync();

            return calculation;
        }

        public async Task<Calculation1> Update(Calculation1 calculation)
        {
            if (calculation == null) return null;

            calculation.CalculationOfSystemGenerationInKwPerDay();
            calculation.CalculationOfSystemGenerationWithBoardPerDay();
            calculation.CalculationOfSystemGenerationWithBoardPerMonth();

            _context.Calculations1.Update(calculation);
            await _context.SaveChangesAsync();
            return calculation;
        }

        public async Task<bool> Delete(int simulationId)
        {
            var calculation = await _context.Calculations1
                .Where(c => c.SimulationId == simulationId)
                .FirstOrDefaultAsync();

            if (calculation == null) return false;

            _context.Calculations1.Remove(calculation);
            await _context.SaveChangesAsync();
            return true;
        }
    }
}
