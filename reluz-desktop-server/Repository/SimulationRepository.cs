﻿using Microsoft.EntityFrameworkCore;
using reluz_desktop_server.Model.Context;
using reluz_desktop_server.Model.SimulationClasses;
using reluz_desktop_server.Model.SimulationClasses.Calculations;
using reluz_desktop_server.Repository.IRepository;

namespace reluz_desktop_server.Repository
{
    public class SimulationRepository : ISimulationRepository
    {
        private readonly MySQLContext _context;
        private readonly ICalculation1Repository _calculation1Repository;

        public SimulationRepository(
            MySQLContext context,
            ICalculation1Repository calculation1Repository)
        {
            _context = context;
            _calculation1Repository = calculation1Repository;
        }

        public async Task<IEnumerable<Simulation>> FindAll()
        {
            List<Simulation> simulations = await _context.Simulations
                .Include(s => s.Calculation1)
                .ToListAsync();

            return simulations;
        }

        public async Task<Simulation> Create(string title, int id)
        {
            var simulation = new Simulation
            {
                Title = title,
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                UserId = id
            };

            await _context.Simulations.AddAsync(simulation);
            await _context.SaveChangesAsync();

            simulation.Calculation1 = await _calculation1Repository.Create(simulation.SimulationId);

            return simulation;
        }

        public async Task<IEnumerable<Simulation>> FindByUserId(long userId)
        {
            var simulations = await _context.Simulations
                .Include(s => s.Calculation1)
                .Where(s => s.UserId == userId)
                .ToListAsync();

            if (simulations == null) return null;
            return simulations;
        }

        public async Task<bool> Delete(int simulationId)
        {
            var simulation = await _context.Simulations
                .Where(s => s.SimulationId == simulationId)
                .FirstOrDefaultAsync();
            
            if (simulation == null) return false;

            var isDeleted = true;

            isDeleted = await _calculation1Repository.Delete(simulation.SimulationId);

            if (!isDeleted) return false;

            _context.Simulations.Remove(simulation);
            await _context.SaveChangesAsync();
            
            return true;
        }

        public async Task<Simulation> UpdateCalculation1(Simulation simulation)
        {
            var simulationToUpdate = _context.Simulations
                .Where(s => s.SimulationId == simulation.SimulationId)
                .FirstOrDefault();

            if (simulationToUpdate == null) return null;

            var calculationUpdated = await _calculation1Repository.Update(simulation.Calculation1);
            simulationToUpdate.Calculation1 = calculationUpdated;

            simulationToUpdate.UpdatedAt = DateTime.Now;
            _context.Simulations.Update(simulationToUpdate);
            await _context.SaveChangesAsync();

            return simulationToUpdate;
        }
    }
}
