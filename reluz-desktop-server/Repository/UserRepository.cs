﻿using Microsoft.EntityFrameworkCore;
using reluz_desktop_server.Model;
using reluz_desktop_server.Model.Context;
using reluz_desktop_server.Repository.IRepository;

namespace reluz_desktop_server.Repository;

public class UserRepository : IUserRepository
{
    private readonly MySQLContext _context;

    public UserRepository(MySQLContext context)
    {
        _context = context;
    }

    public async Task<IEnumerable<User>> FindAll()
    {
        List<User> users = await _context.Users.ToListAsync();

        return users;
    }

    public async Task<User> FindById(long id)
    {
        User user = await _context.Users.Where(u => u.Id == id).FirstOrDefaultAsync();

        if (user == null) return null;
        return user;
    }

    public async Task<User> FindByUserName(string userName)
    {
        User user = await _context.Users.Where(u => u.UserName == userName).FirstOrDefaultAsync();

        if (user == null) return null;
        return user;
    }
    
    public async Task<User> Create(User user)
    {
        if (user == null) return null;
        
        user.SetHashPassoword();
        _context.Users.Add(user);
        await _context.SaveChangesAsync();
        return user;
    }

    public async Task<User> Update(User user)
    {
        if (user == null) return null;

        _context.Users.Update(user);
        await _context.SaveChangesAsync();
        return user;
    }

    public async Task<bool> Delete(User user)
    {
        if (user == null) return false;

        _context.Users.Remove(user);
        await _context.SaveChangesAsync();
        return true;
    }
}
