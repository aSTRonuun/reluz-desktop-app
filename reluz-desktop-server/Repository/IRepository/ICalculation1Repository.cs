﻿using reluz_desktop_server.Model.SimulationClasses;
using reluz_desktop_server.Model.SimulationClasses.Calculations;

namespace reluz_desktop_server.Repository.IRepository
{
    public interface ICalculation1Repository
    {

        Task<Calculation1> FindBySimulationId(int simulationId);
        Task<Calculation1> Create(int simulationId);
        Task<Calculation1> Update(Calculation1 calculation);
        Task<bool> Delete(int id);
    }
}
