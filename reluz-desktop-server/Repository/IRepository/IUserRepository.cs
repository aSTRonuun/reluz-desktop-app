﻿using reluz_desktop_server.Model;

namespace reluz_desktop_server.Repository.IRepository;

public interface IUserRepository
{
    Task<IEnumerable<User>> FindAll();

    Task<User> FindById(long id);

    Task<User> FindByUserName(string userName);

    Task<User> Create(User user);

    Task<User> Update(User user);

    Task<bool> Delete(User user);
}
