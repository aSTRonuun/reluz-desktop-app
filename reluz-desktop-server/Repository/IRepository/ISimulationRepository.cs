﻿using reluz_desktop_server.Model;
using reluz_desktop_server.Model.SimulationClasses;

namespace reluz_desktop_server.Repository.IRepository
{
    public interface ISimulationRepository
    {
        Task<IEnumerable<Simulation>> FindAll();

        Task<IEnumerable<Simulation>> FindByUserId(long userId);

        Task<Simulation> Create(string title, int id);

        Task<Simulation> UpdateCalculation1(Simulation simulation);
        
        Task<bool> Delete(int simulationId);
    }
}
