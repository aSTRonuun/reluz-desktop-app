﻿using reluz_desktop_server.Data.Dtos;

namespace reluz_desktop_server.Business.IBusiness;

public interface IUserBusiness
{
    Task<IEnumerable<UserDto>> FindAll();
    Task<UserDto> FindById(long id);
    Task<UserDto> FindByUserName(string userName, string password);
    Task<UserDto> Create(UserDto dto);
    Task<UserDto> Update(UserUpdateDto dto);
    Task<bool> Delete(UserDto dto);
}
