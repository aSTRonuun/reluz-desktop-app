﻿using reluz_desktop_server.Data.Dtos;
using reluz_desktop_server.Model.SimulationClasses;

namespace reluz_desktop_server.Business.IBusiness
{
    public interface ISimulationBusiness
    {
        Task<IEnumerable<SimulationDto>> FindAll();
        Task<IEnumerable<SimulationDto>> FindByUserId(long userId);
        Task<SimulationDto> Create(string title, int id);
        Task<Simulation> UpdateCalculation1(Simulation simulation);
        Task<bool> Delete(int simulationId);
    }
}
