﻿using AutoMapper;
using reluz_desktop_server.Business.IBusiness;
using reluz_desktop_server.Data.Dtos;
using reluz_desktop_server.Model.SimulationClasses;
using reluz_desktop_server.Model.SimulationClasses.Calculations;
using reluz_desktop_server.Repository.IRepository;

namespace reluz_desktop_server.Business
{
    public class SimulationBusiness : ISimulationBusiness
    {
        private ISimulationRepository _repository;
        private IMapper _mapper;

        public SimulationBusiness(ISimulationRepository repository, IMapper mapper)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<IEnumerable<SimulationDto>> FindAll()
        {
            var simulation = await _repository.FindAll();
            return _mapper.Map<IEnumerable<SimulationDto>>(simulation);
        }
        public async Task<IEnumerable<SimulationDto>> FindByUserId(long userId)
        {
            var simulation = await _repository.FindByUserId(userId);
            return _mapper.Map<IEnumerable<SimulationDto>>(simulation);
        }

        public async Task<SimulationDto> Create(string title, int id)
        {
            var simulation = await _repository.Create(title, id);

            if (simulation == null) return null;
            return _mapper.Map<SimulationDto>(simulation);
        }

        public Task<bool> Delete(int simulationId)
        {
            var result = _repository.Delete(simulationId);
            return result;
        }
        public async Task<Simulation> UpdateCalculation1(Simulation simulation)
        {
            var result = await _repository.UpdateCalculation1(simulation);
            return result;
        }
    }
}
