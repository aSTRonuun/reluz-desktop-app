﻿using AutoMapper;
using reluz_desktop_server.Business.IBusiness;
using reluz_desktop_server.Data.Dtos;
using reluz_desktop_server.Model;
using reluz_desktop_server.Repository.IRepository;

namespace reluz_desktop_server.Business;

public class UserBusiness : IUserBusiness
{
    private IUserRepository _repository;
    private IMapper _mapper;

    public UserBusiness(IUserRepository repository, IMapper mapper)
    {
        _repository = repository ?? throw new ArgumentNullException(nameof(repository));
        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
    }
    
    public async Task<IEnumerable<UserDto>> FindAll()
    {
        var users = await _repository.FindAll();
        return _mapper.Map<IEnumerable<UserDto>>(users);
    }

    public async Task<UserDto> FindById(long id)
    {
        var result = await _repository.FindById(id);

        if (result == null) return null;
        return _mapper.Map<UserDto>(result);
    }

    public async Task<UserDto> FindByUserName(string userName, string password)
    {
        var user = await _repository.FindByUserName(userName);

        if (user == null) return null;

        if (user.ValidatePassword(password))
        {
            return _mapper.Map<UserDto>(user);
        }
        else
        {
            return null;
        }
    }

    public async Task<UserDto> Create(UserDto dto)
    {
        User user = _mapper.Map<User>(dto);

        var result = await _repository.Create(user);
        if (result == null) return null;
        return _mapper.Map<UserDto>(result);

    }
    
    public async Task<UserDto> Update(UserUpdateDto dto)
    {
        User user;
        if(dto.NewPassword == null)
        {
            user = _mapper.Map<User>(dto);
        } else
        {
            user = await _repository.FindById(dto.Id);
            if (user == null) return null;

            if (user.ValidatePassword(dto.Password))
            {
                user.UserName = dto.UserName;
                user.Password = dto.NewPassword;
                user.SetHashPassoword();
            }
            else
            {
                return null;
            }
        }

        var result = await _repository.Update(user);
        if (result == null) return null;
        return _mapper.Map<UserDto>(result);
    }

    public async Task<bool> Delete(UserDto dto)
    {
        User user = _mapper.Map<User>(dto);

        var result = await _repository.FindByUserName(user.UserName);
        if (result == null) return false;
        
        if (result.ValidatePassword(user.Password))
        {
            return await _repository.Delete(user);
        }
        return false;
    }
}
