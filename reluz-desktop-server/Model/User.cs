﻿using reluz_desktop_server.Model.Base;
using reluz_desktop_server.Service.Cryptography;
using System.ComponentModel.DataAnnotations.Schema;

namespace reluz_desktop_server.Model;

[Table("User")]
public class User : BaseEntity
{
    public string UserName { get; set; }
    public string Password { get; set; }

    public void SetHashPassoword()
    {
        Password = Password.GenerateHash();
    }

    public bool ValidatePassword(string password)
    {
        return Password == password.GenerateHash();
    }
}
