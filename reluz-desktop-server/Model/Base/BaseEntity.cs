﻿namespace reluz_desktop_server.Model.Base;

public class BaseEntity
{
    public long Id { get; set; }
}
