﻿using reluz_desktop_server.Model.Base;
using reluz_desktop_server.Model.SimulationClasses.Calculations;
using System.ComponentModel.DataAnnotations.Schema;

namespace reluz_desktop_server.Model.SimulationClasses;

[Table("simulation")]
public class Simulation
{
    public int SimulationId { get; set; }
    public string Title { get; set; }
    public DateTime CreatedAt { get; set; }
    public DateTime UpdatedAt { get; set; }
    public int UserId { get; set; }
    public Calculation1 Calculation1 { get; set; }
}
