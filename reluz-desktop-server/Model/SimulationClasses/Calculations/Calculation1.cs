﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace reluz_desktop_server.Model.SimulationClasses.Calculations;

[Table("Generation_of_the_solar_energy_system_in_kw_simulation")]
public class Calculation1
{
    [Key]
    public int Id { get; set; }
    public string Title { get; set; }
    public string Descriptionn { get; set; }
    public int SimulationId { get; set; }
    public double? AverageConsumptionInReais { get; set; }
    public double? SumConsumptionInKwPerMonth { get; set; }
    public double? SystemGenerationInKwPerDay { get; set; }
    public double? OverallIncome { get; set; }
    public double? SolarRadiationIndex { get; set; }
    public double? SystemGenerationWithBoardPerDay { get; set; }
    public double? SystemGenerationWithBoardPerMonth { get; set; }

    public void CalculationOfSystemGenerationInKwPerDay()
    {
        SystemGenerationInKwPerDay = SumConsumptionInKwPerMonth / 360;
    }

    public void CalculationOfSystemGenerationWithBoardPerDay()
    {
        SystemGenerationWithBoardPerDay = SystemGenerationInKwPerDay / (OverallIncome * SolarRadiationIndex);
    }

    public void CalculationOfSystemGenerationWithBoardPerMonth()
    {
        SystemGenerationWithBoardPerMonth = SystemGenerationWithBoardPerDay * 30;
    }
}
