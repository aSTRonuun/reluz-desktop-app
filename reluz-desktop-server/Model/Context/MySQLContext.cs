﻿using Microsoft.EntityFrameworkCore;
using reluz_desktop_server.Model.SimulationClasses;
using reluz_desktop_server.Model.SimulationClasses.Calculations;

namespace reluz_desktop_server.Model.Context;

public class MySQLContext : DbContext
{
    public MySQLContext() { }

    public MySQLContext(DbContextOptions<MySQLContext> options) : base(options) { }

    public DbSet<User> Users { get; set; }

    public DbSet<Simulation> Simulations { get; set; }

    public DbSet<Calculation1> Calculations1 { get; set; }
}
