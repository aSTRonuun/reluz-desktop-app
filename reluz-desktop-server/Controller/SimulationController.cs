﻿using Microsoft.AspNetCore.Mvc;
using reluz_desktop_server.Business.IBusiness;
using reluz_desktop_server.Data.Dtos;
using reluz_desktop_server.Model;
using reluz_desktop_server.Model.SimulationClasses;
using reluz_desktop_server.Repository.IRepository;

namespace reluz_desktop_server.Controller;

[Route("api/[controller]")]
[ApiController]
public class SimulationController : ControllerBase
{
    private ISimulationBusiness _business;

    public SimulationController(ISimulationBusiness business)
    {
        _business = business;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<SimulationDto>>> FindAll()
    {
        var simulations = await _business.FindAll();

        if (simulations == null) return NotFound();

        return Ok(simulations);
    }

    [HttpGet("FindUserId/{userId}")]
    public async Task<ActionResult<IEnumerable<SimulationDto>>> FindByUserId(long userId)
    {
        if (userId == null) return BadRequest();
        
        var simulations = await _business.FindByUserId(userId);

        if (simulations == null) return NotFound();

        return Ok(simulations);
    }

    [HttpPost]
    public async Task<ActionResult<SimulationDto>> Create([FromBody] SimulationDto dto)
    {
        if (dto == null) return BadRequest();
        
        var simulation = await _business.Create(dto.Title, dto.UserId);

        if (simulation == null) return BadRequest();

        return Ok(simulation);
    }

    [HttpDelete("{simulationId}")]
    public async Task<ActionResult<bool>> Delete(int simulationId)
    {
        if (simulationId == null) return BadRequest();

        var isDeleted = await _business.Delete(simulationId);

        if (!isDeleted) return NotFound(isDeleted);

        return Ok(isDeleted);
    }

    [HttpPut("UpdateCalculation1")]
    public async Task<ActionResult<Simulation>> UpdateCalculation1([FromBody] Simulation simulation)
    {
        if (simulation == null) return BadRequest();

        var result = await _business.UpdateCalculation1(simulation);

        if (result == null) return BadRequest();

        return Ok(result);
    }
}
