﻿using Microsoft.AspNetCore.Mvc;
using reluz_desktop_server.Business.IBusiness;
using reluz_desktop_server.Data.Dtos;

namespace reluz_desktop_server.Controller;

[Route("api/[controller]")]
[ApiController]
public class UserController : ControllerBase
{
    private IUserBusiness _business;

    public UserController(IUserBusiness business)
    {
        _business = business;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<UserDto>>> FindAll()
    {
        var users = await _business.FindAll();

        return Ok(users);
    }

    [HttpGet("FindId/")]
    public async Task<ActionResult<UserDto>> FindById(long id)
    {
        var user = await _business.FindById(id);

        if (user == null) return NotFound();
        return Ok(user);
    }
    
    [HttpPost("Login")]
    public async Task<ActionResult<UserDto>> FindByUserName([FromBody] UserDto dto)
    {
        var user = await _business.FindByUserName(dto.UserName, dto.Password);

        if (user == null) return NotFound();
        return Ok(user);
    }

    [HttpPost]
    public async Task<ActionResult<UserDto>> Create([FromBody] UserDto dto)
    {
        var user = await _business.Create(dto);
        if (user == null) return BadRequest();
        return Ok(user);
    }

    [HttpPut]
    public async Task<ActionResult<UserDto>> Update([FromBody] UserUpdateDto dto)
    {
        var user = await _business.Update(dto);
        if (user == null) return BadRequest();
        return Ok(user);
    }

    [HttpDelete]
    public async Task<ActionResult<bool>> Delete([FromBody] UserDto dto)
    {
        var result = await _business.Delete(dto);
        if (result == false) return BadRequest();
        return Ok(result);
    }
}
