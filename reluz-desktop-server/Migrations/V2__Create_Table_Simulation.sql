CREATE TABLE IF NOT EXISTS `Simulation` (
    `SimulationId` INTEGER NOT NULL AUTO_INCREMENT,
	`Title` varchar(255) NOT NULL,
	`CreatedAt` datetime NOT NULL,
	`UpdatedAt` datetime NOT NULL,
	`UserId` INTEGER NOT NULL,
	PRIMARY KEY (`SimulationId`),
	FOREIGN KEY (`UserID`) REFERENCES `User`(`Id`)
) 