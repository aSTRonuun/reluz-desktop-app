﻿CREATE TABLE IF NOT EXISTS `Generation_of_the_solar_energy_system_in_kw_simulation` (
    `Id` INTEGER NOT NULL AUTO_INCREMENT,
	`Title` varchar(255) NOT NULL,
	`SimulationId` INTEGER NOT NULL,
	`Descriptionn` varchar(255) NOT NULL,
	`AverageConsumptionInReais` Double,
	`SumConsumptionInKwPerMonth` Double,
	`SystemGenerationInKwPerDay` Double,
	`OverallIncome` Double,
	`SolarRadiationIndex` Double,
	`SystemGenerationWithBoardPerDay` Double,
	`SystemGenerationWithBoardPerMonth` Double,
    PRIMARY KEY (`Id`),
	FOREIGN KEY (`SimulationId`) REFERENCES `Simulation`(`SimulationId`)
) 