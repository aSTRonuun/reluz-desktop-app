﻿using reluz_desktop_server.Service.Cryptography;

namespace reluz_desktop_server_tests.ServicesTests
{
    [TestClass]
    public class CryptographyTests
    {
        [TestMethod]
        public void ShouldReturnHashedValue()
        {
            // Arrange
            var value = "test";
            var expected = "a94a8fe5ccb19ba61c4c0873d391e987982fbbd3";

            // Act
            var result = value.GenerateHash();

            // Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void ShouldThrowArgumentNullExcpetion()
        {
            {
                // Arrange
                var value = string.Empty;
                var expected = new ArgumentException(nameof(value)).Message;

                // Act
                var result = value.GenerateHash();

                // Assert
                Assert.AreEqual(expected, result);
            }
        }
    }
}
