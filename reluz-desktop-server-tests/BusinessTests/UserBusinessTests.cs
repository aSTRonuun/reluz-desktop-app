using AutoMapper;
using NSubstitute;
using reluz_desktop_server.Business;
using reluz_desktop_server.Data.Dtos;
using reluz_desktop_server.Model;
using reluz_desktop_server.Repository.IRepository;

namespace reluz_desktop_server_tests;

[TestClass]
public class UserBusinessTests
{
    [TestMethod]
    public void ShouldReturnNullWhenNotCreatedSuccessfully()
    {
        // Arrange
        var mapper = new MapperConfiguration(cfg => cfg.CreateMap<UserDto, User>().ReverseMap()).CreateMapper();

        var userBusiness = CreateUserBusiness(mapper);
        var userDto = new UserDto
        {
            UserName = "test",
            Password = "test"
        };

        // Act
        var result = userBusiness.Create(new UserDto { UserName = "test", Password = "test" });

        // Assert
        Assert.IsNull(result.Result);
    }

    [TestMethod]
    public void ShouldCreatedSuccessfully()
    {

        var mapper = new MapperConfiguration(cfg => cfg.CreateMap<UserDto, User>().ReverseMap()).CreateMapper();
        var userRepository = Substitute.For<IUserRepository>();

        userRepository.Create(Arg.Any<User>()).Returns(new User { Id = 1, UserName = "test", Password = "test" });

        // Arrange
        var userBusiness = new UserBusiness(userRepository, mapper);
        
        var expected = new UserDto
        {
            UserName = "test",
            Password = "test"
        };

        // Act
        var result = userBusiness.Create(new UserDto { UserName = "test", Password = "test" });

        // Assert
        Assert.IsNotNull(result);
    }

    [TestMethod]
    public void ShouldDeletUserSuccessfully()
    {
        // Arrange
        var mapper = new MapperConfiguration(cfg => cfg.CreateMap<UserDto, User>().ReverseMap()).CreateMapper();
        var userRepository = Substitute.For<IUserRepository>();
        userRepository.FindByUserName(Arg.Any<string>()).Returns(new User { Id = 1, UserName = "test", Password = "a94a8fe5ccb19ba61c4c0873d391e987982fbbd3" });
        userRepository.Delete(Arg.Any<User>()).Returns(true);

        var userBusiness = new UserBusiness(userRepository, mapper);

        // Act
        var result = userBusiness.Delete(new UserDto { UserName = "test", Password = "test" });

        // Assert
        Assert.IsTrue(result.Result);
    }

    private UserBusiness CreateUserBusiness(IMapper mapper = null, IUserRepository userRepository = null)
    {
        return new UserBusiness(
            userRepository ?? Substitute.For<IUserRepository>(),
            mapper ?? Substitute.For<IMapper>());
    }
}
