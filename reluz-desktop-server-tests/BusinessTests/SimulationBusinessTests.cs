﻿using AutoMapper;
using reluz_desktop_server.Business;
using reluz_desktop_server.Data.Dtos;
using reluz_desktop_server.Model.SimulationClasses;

namespace reluz_desktop_server_tests.BusinessTests;

[TestClass]
public class SimulationBusinessTests
{
    [TestMethod]
    public void ShouldCreateSimulationWithCalculation1Correctly()
    {
        // Arrange
        var mapper = new MapperConfiguration(cfg => cfg.CreateMap<SimulationDto, Simulation>().ReverseMap()).CreateMapper();

        var expectSimulation = new SimulationDto
        {
            Title = "test",
            calculation1 = new Calculation1Dto
            {
                Title = "test",
                Descriptionn = "test",
                OverallIncome = 0.7,
            }
        };
        
        

        var simulationBusiness = CreateSimulationBusiness(mapper);
    }

    private SimulationBusiness CreateSimulationBusiness(IMapper mapper)
    {
        return new SimulationBusiness(null, mapper);
    }
}
